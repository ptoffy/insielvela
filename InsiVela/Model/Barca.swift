//
//  Barca.swift
//  InsielVela
//
//  Created by Mac04 on 23/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct Barca: Codable {
    
    var nome: String
    var modello: String
    var categoria: String
    var lunghezza: String
    var idArmatore: String
    
    init(nome: String, modello: String, categoria: String, lunghezza: String, idArmatore: String) {
        self.nome = nome
        self.modello = modello
        self.categoria = categoria
        self.lunghezza = lunghezza
        self.idArmatore = idArmatore
    }
    
}
