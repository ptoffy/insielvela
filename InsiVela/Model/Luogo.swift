//
//  Luogo.swift
//  InsielVela
//
//  Created by Paul Toffoloni on 26/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct Luogo {
    
    var longitudine: Double
    var latitudine: Double
    var ora: Date
    
    init(longitudine: Double, latitudine: Double, ora: Date) {
        self.longitudine = longitudine
        self.latitudine = latitudine
        self.ora = ora
    }
    
}
