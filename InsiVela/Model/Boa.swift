//
//  Boa.swift
//  InsielVela
//
//  Created by Paul Toffoloni on 26/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct Boa {
    
    var latitudine: String?
    var longitudine: String?
    var anno: String?
    var idBoa: String?
    
}
