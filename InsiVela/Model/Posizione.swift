//
//  Posizione.swift
//  InsiVela
//
//  Created by Paul Toffoloni on 06/02/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct Posizione: Codable {
    
    var longitudine: String?
    var latitudine: String?
    var ora: String?
    
}
