//
//  Utente.swift
//  InsielVela
//
//  Created by Mac04 on 21/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct Utente: Codable {
    
    var email: String
    var nome: String
    var cognome: String
    var dataScadenzaCertificato: Date?
    var idSkipper: String
    
    init(email: String, nome: String, cognome: String, dataScadenzaCertificato: String, idSkipper: String) {
        self.email = email
        self.nome = nome
        self.cognome = cognome
        self.dataScadenzaCertificato = Format.date(dataScadenzaCertificato)
        self.idSkipper = idSkipper
    }
}
