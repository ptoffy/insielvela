//
//  Gara.swift
//  InsielVela
//
//  Created by Paul Toffoloni on 26/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct Regata: Codable {
    
    var anno: String
    var data: String
    var oraInizio: String
    var oraFine: String
    var tempoAggiornamentoProprio: String
    var tempoAggiornamentoAltrui: String
    
}
