//
//  CharacterSet.swift
//  InsielVela
//
//  Created by Paul Toffoloni on 26/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

extension CharacterSet {
    
    ///Rimuove caratteri non accettati
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@"
        let subDelimitersToEncode = "!$&'()*+,;="
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
