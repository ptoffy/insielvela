//
//  DateTime.swift
//  InsielVela
//
//  Created by Paul Toffoloni on 30/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

class DateTime {

    public static func currentDate() -> String {
        let currentDate = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.string(from: currentDate)
    }

    public static func currentTime() -> String {
        let currentTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        return formatter.string(from: currentTime)
    }
    
    public static func getDateDiff(start: Date, end: Date) -> DateComponents  {
        return Calendar.current.dateComponents([.month, .day, .hour, .minute, .second], from: start, to: end)
    }

}


