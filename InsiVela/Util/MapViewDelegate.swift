//
//  MapViewDelegate.swift
//  InsiVela
//
//  Created by Paul Toffoloni on 05/02/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import MapKit

/// Map View Delegate
extension MapViewController: MKMapViewDelegate {
    ///Crea la linea da disegnare
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        guard let polyline = overlay as? MKPolyline else {
            return MKOverlayRenderer(overlay: overlay)
        }
        
        let polylineRenderer = MKPolylineRenderer(polyline: polyline)
        polylineRenderer.strokeColor = .blue
        polylineRenderer.lineWidth = 3
        return polylineRenderer
    }
}
