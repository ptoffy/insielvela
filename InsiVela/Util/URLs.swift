//
//  URLs.swift
//  InsielVela
//
//  Created by Mac04 on 27/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import Foundation

struct URLs {
    //URL per la connessione
    public static let urlScuola: String = "http://192.168.105.20/5F/toffoloni/insiel/"
    public static let urlCasa: String = "http://192.168.1.244/~paultoffy/Insiel/"
    public static let urlRemoto: String = "http://andreaivkovic.it/toffoloni/InsiVela/"

    public static let activeUrl: String = urlRemoto
    
    //URL per i file
    public static let loginFileName: String = "login.php"
    public static let barcheFileName: String = "barca.php"
    public static let regataFileName: String = "regata.php"
    public static let inviaPosizioneFileName: String = "invioPosizione.php"
    public static let riceviPosizioneFileName: String = "riceviPosizione.php"
    public static let listaUtentiFileName: String = "listaUtenti.php"
    public static let listaBarcheFileName: String = "listaBarche.php"
    
}
