//
//  CountdownViewController.swift
//  InsiVela
//
//  Created by Paul Toffoloni on 02/02/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit

class CountdownViewController: UIViewController {
    
    @IBOutlet weak var monthTimer: UILabel!
    @IBOutlet weak var dayTimer: UILabel!
    @IBOutlet weak var hourTimer: UILabel!
    @IBOutlet weak var minuteTimer: UILabel!
    @IBOutlet weak var secondTimer: UILabel!
    
    let futureDate: Date = {
        var future = DateComponents(
            year: 2021,
            month: 1,
            day: 1,
            hour: 0,
            minute: 0,
            second: 0
        )
        return Calendar.current.date(from: future)!
    }()
    
    let regata = try? PropertyListDecoder().decode(Regata.self, from: UserDefaults.standard.object(forKey: "regata") as! Data)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.hidesBackButton = true
        
        aggiornaDati()
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(aggiornaDati), userInfo: nil, repeats: true)
//        _ = Timer(fireAt: Format.dateWithTime(date: self.regata!.data, time: self.regata!.oraInizio), interval: 0, target: self, selector: #selector(switchView), userInfo: nil, repeats: false)

    }
    
    @objc func aggiornaDati() {
        let components = DateTime.getDateDiff(start: Date(), end: Format.dateWithTime(date: self.regata!.data, time: self.regata!.oraInizio))
//        let components = DateTime.getDateDiff(start: Date(), end: futureDate)
        
        let monthDiff = components.month ?? 0
        let dayDiff = components.day ?? 0
        let hourDiff = components.hour ?? 0
        let minuteDiff = components.minute ?? 0
        let secondDiff = components.second ?? 0
        
        monthTimer.text = "\(monthDiff) mesi"
        dayTimer.text = "\(dayDiff) giorni"
        hourTimer.text = "\(hourDiff) ore"
        minuteTimer.text = "\(minuteDiff) minuti"
        secondTimer.text = "\(secondDiff) secondi"
    }
    
    @objc func switchView() {
        let destViewController = storyBoard.instantiateViewController(withIdentifier: "mapViewController") as! MapViewController
        self.navigationController?.pushViewController(destViewController, animated: true)
    }

}
