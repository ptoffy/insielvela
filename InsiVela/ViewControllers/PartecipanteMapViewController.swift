//
//  PartecipanteMapViewController.swift
//  InsiVela
//
//  Created by Paul Toffoloni on 05/02/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit
import MapKit

class PartecipanteMapViewController: UIViewController {

    @IBOutlet weak var nomeCognomeLabel: UILabel!
    @IBOutlet weak var velocitàLabel: UILabel!
    @IBOutlet weak var nomeBarcaLabel: UILabel!
    @IBOutlet weak var tempoTrascorsoLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    private let restManager = RestManager()
    
    private var locationList: [CLLocation] = []
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    
    var utente: Utente?
    var barca: Barca?
    private let regata = try? PropertyListDecoder().decode(Regata.self, from: UserDefaults.standard.object(forKey: "regata") as! Data)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _ = Timer.scheduledTimer(timeInterval: Double(self.regata!.tempoAggiornamentoAltrui) ?? 30.0, target: self, selector: #selector(getPosizione), userInfo: nil, repeats: true)
    }
    
    func controllaStringaDiRisposta(response: String) -> Bool {
        if response.contains("invalid") {
            return false
        } else {
            return true
        }
    }
    
    @objc func getPosizione() {
        let url = URL(string: URLs.activeUrl + URLs.riceviPosizioneFileName)
        
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        restManager.httpBodyParameters.add(value: self.utente!.idSkipper, forKey: "idSkipper")
        
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if let data = results.data {
                guard self.controllaStringaDiRisposta(response: String(data: data, encoding: .utf8)!) else { return }
                let decoder = JSONDecoder()
                let posizione = try? decoder.decode(Posizione.self, from: data)
                let location = CLLocation(latitude: CLLocationDegrees(exactly: Double((posizione!.latitudine)!)!)!, longitude: CLLocationDegrees(exactly: Double((posizione!.longitudine)!)!)!)
                self.disegnaPosizione(newLocation: location)
            }
        })
    }
    
    func disegnaPosizione(newLocation: CLLocation) {
        if let lastLocation = locationList.last {
            let delta = newLocation.distance(from: lastLocation)
            distance = distance + Measurement(value: delta, unit: UnitLength.meters)
            //Disegna la linea delle coordinate
            let coordinates = [lastLocation.coordinate, newLocation.coordinate]
            mapView.addOverlay(MKPolyline(coordinates: coordinates, count: 2))
            //Centra la visuale sul cursore
            let region = MKCoordinateRegion(center: newLocation.coordinate, latitudinalMeters: 500, longitudinalMeters: 500)
            mapView.setRegion(region, animated: true)
        }
        locationList.append(newLocation)
    }

}
