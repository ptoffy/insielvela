//
//  SettingsViewController.swift
//  InsielVela
//
//  Created by Paul Toffoloni on 30/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    
    @IBAction func logout(_ sender: Any) {
        UserDefaults.standard.set("", forKey: "utente")
        DispatchQueue.main.async {
            let destViewController = self.storyboard?.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
    }
    
}
