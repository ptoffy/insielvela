//
//  LaunchViewController.swift
//  InsiVela
//
//  Created by Paul Toffoloni on 31/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setImageView()

        if !UserDefaults.standard.bool(forKey: "logged"){
            DispatchQueue.main.async {
                let destViewController = storyBoard.instantiateViewController(withIdentifier: "loginViewController") as! LoginViewController
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
        } else {
            DispatchQueue.main.async {
                let destViewController = storyBoard.instantiateViewController(withIdentifier: "countdownViewController") as! CountdownViewController
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
        }
    }
    
    private func setImageView() {
        self.imageView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }

}
