//
//  PartecipantiTableViewController.swift
//  InsiVela
//
//  Created by Paul Toffoloni on 30/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit

class PartecipantiTableViewController: UITableViewController {
    
    let restManager = RestManager()
    var listaBarche: [Barca] = []
    var listaUtenti: [Utente] = []
    var listaCategorie: [String] = []
    
    let utenteLoggato = try? JSONDecoder().decode(Utente.self, from: UserDefaults.standard.object(forKey: "utente") as! Data)
    
    //Numero di barche per categoria
    var barchePerCategoria: [String: Int] = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        initListaCategorie()
        trovaBarcheDiCategoria()
    }
    
    func initListaCategorie() {
        for barca in self.listaBarche {
            if !listaCategorie.contains(barca.categoria) {
                listaCategorie.append(barca.categoria)
            }
        }
    }


    // MARK: - TableView Data Source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return listaCategorie.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return listaCategorie[section]
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return barchePerCategoria[listaCategorie[section]]!
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.register(PartecipantiTableViewCell.self, forCellReuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PartecipantiTableViewCell

        if !(listaUtenti[indexPath.row].idSkipper == utenteLoggato!.idSkipper) {
            let utente = listaUtenti[indexPath.row]
            let barca = listaBarche[indexPath.row]
            cell.utente = utente
            cell.barca = barca
            cell.textLabel?.text = utente.nome + " " + utente.cognome
            cell.detailTextLabel?.text = barca.nome
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! PartecipantiTableViewCell
        DispatchQueue.main.async {
            let destViewController = storyBoard.instantiateViewController(withIdentifier: "partecipanteMapViewController") as! PartecipanteMapViewController
            destViewController.barca = cell.barca
            destViewController.utente = cell.utente
            self.navigationController?.pushViewController(destViewController, animated: true)
        }
    }
    
    func trovaBarcheDiCategoria() {
        for categoria in listaCategorie {
            barchePerCategoria[categoria] = 0
        }
        for barca in listaBarche {
            switch barca.categoria {
            case listaCategorie[0]:
                barchePerCategoria[listaCategorie[0]]! += 1
            case listaCategorie[1]:
                barchePerCategoria[listaCategorie[1]]! += 1
            default:
                return
            }
        }
    }
}

class PartecipantiTableViewCell: UITableViewCell {

    var utente: Utente?
    var barca: Barca?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
