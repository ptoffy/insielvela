//
//  ViewController.swift
//  InsielVela
//
//  Created by Mac04 on 21/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    private var responseString = ""
    
    private var utente: Utente?
    private var regata: Regata?
    
    let restManager = RestManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.layer.borderColor = UIColor(red:0.28, green:0.49, blue:0.90, alpha:1.0).cgColor
        emailTextField.layer.borderWidth = 1.5
        passwordTextField.layer.borderColor = UIColor(red:0.28, green:0.49, blue:0.90, alpha:1.0).cgColor
        passwordTextField.layer.borderWidth = 1.5
                
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil);

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil);
        
        self.navigationItem.hidesBackButton = true

        self.hideKeyboardWhenTap()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        ottieniDatiRegata()
    }
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        self.richiediDatiLoginECambiaSchermata()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -120
    }
    
    @objc func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }
    
    ///Controlla se la stringa in risposta è valida.
    func controllaStringaDiRisposta(response: String, sender: Int) -> Bool {
        if response.contains("message") || response.contains("invalid") {
                switch sender {
                case 1 :
                    DispatchQueue.main.async { self.errorLabel.text = "Non sono state trovate barche" }
                case 2 :
                    DispatchQueue.main.async { self.errorLabel.text = "Utente o password non corretti" }
                case 3 :
                    DispatchQueue.main.async { self.errorLabel.text = "Non ci sono regate quest'anno!" }
                default:
                    return false
            }
             return false
         } else {
            return true
        }
    }
    
    ///Cambia la schermata usando il thread principale.
    func cambiaSchermata() {
        if Date() < Format.dateWithTime(date: regata!.data, time: regata!.oraInizio) {
                DispatchQueue.main.async {
                let destViewController = storyBoard.instantiateViewController(withIdentifier: "countdownViewController") as! CountdownViewController
                self.navigationController?.pushViewController(destViewController, animated: true)
                }
        } else {
            DispatchQueue.main.async {
                let destViewController = storyBoard.instantiateViewController(withIdentifier: "mapViewController") as! MapViewController
                self.navigationController?.pushViewController(destViewController, animated: true)
            }
        }
    }
    
    func richiediDatiLoginECambiaSchermata() {
        if emailTextField.text != "" {
            if passwordTextField.text != "" {
                
                ottieniUtente()

            } else {
                errorLabel.text = "Password non valida"
            }
        } else {
            errorLabel.text = "Email non valida"
        }
    }
    
    
    //MARK: Requests
    
    
    ///Manda una richiesta chiedendo dati della barca.
    func ottieniBarca() {
        let url = URL(string: URLs.activeUrl + URLs.barcheFileName)
        
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        restManager.httpBodyParameters.add(value: self.utente!.idSkipper, forKey: "idSkipper")
        
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if let data = results.data {
                guard self.controllaStringaDiRisposta(response: String(data: data, encoding: .utf8)!, sender: 1) else { return }
                let decoder = JSONDecoder()
                let barca = try! decoder.decode(Barca.self, from: data)
                UserDefaults.standard.set(try? PropertyListEncoder().encode(barca), forKey: "barca")
                sleep(1)
                self.cambiaSchermata()
            }
        })
    }
    
    ///Manda una richiesta di login.
    func ottieniUtente() {
        let url = URL(string: URLs.activeUrl + URLs.loginFileName)
        
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        restManager.httpBodyParameters.add(value: emailTextField.text!, forKey: "myusername")
        restManager.httpBodyParameters.add(value: passwordTextField.text!, forKey: "mypassword")
        
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if let data = results.data {
                let response = String(data: data, encoding: .utf8)
                if !self.controllaStringaDiRisposta(response: response!, sender: 2) {
                    print("Invalido")
                }
                else {
                    DispatchQueue.main.async {
                        self.loginButton.isEnabled = false
                        let decoder = JSONDecoder()
                        self.utente = try! decoder.decode(Utente.self, from: data)
                        UserDefaults.standard.set(data, forKey: "utente")
                        UserDefaults.standard.set(true, forKey: "logged")
                        sleep(1)
                        self.ottieniBarca()
                    }
                }
            }
        })
    }
    
    func ottieniDatiRegata() {
        let urls = URLs.activeUrl + URLs.regataFileName
        let url = URL(string: urls)
        let year = String(Calendar.current.component(.year, from: Date()))
        
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        restManager.httpBodyParameters.add(value: year, forKey: "anno")
        
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if let data = results.data {
                guard self.controllaStringaDiRisposta(response: String(data: data, encoding: .utf8)!, sender: 3) else { return }
                let decoder = JSONDecoder()
                self.regata = try! decoder.decode(Regata.self, from: data)
                UserDefaults.standard.set(try? PropertyListEncoder().encode(self.regata), forKey: "regata")
            }
        })
    }
 }
