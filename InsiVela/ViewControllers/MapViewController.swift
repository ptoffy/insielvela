//
//  MapViewController.swift
//  InsielVela
//
//  Created by Mac04 on 21/01/2020.
//  Copyright © 2020 Paul Toffoloni. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    private var restManager = RestManager()
    
    @IBOutlet weak var labelNomeCognome: UILabel!
    @IBOutlet weak var labelNomeBarca: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    private var timer: Timer?
    private var gara: Regata?
    private var locationManager: CLLocationManager!
    private var locationList: [CLLocation] = []
    private var distance = Measurement(value: 0, unit: UnitLength.meters)
    private var tempo: Int = 0
    
    let utente = try? JSONDecoder().decode(Utente.self, from: UserDefaults.standard.object(forKey: "utente") as! Data)
    let barca = try? PropertyListDecoder().decode(Barca.self, from: UserDefaults.standard.object(forKey: "barca") as! Data)
    let regata = try? PropertyListDecoder().decode(Regata.self, from: UserDefaults.standard.object(forKey: "regata") as! Data)

    var listaUtenti: [Utente] = []
    var listaBarche: [Barca] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelNomeCognome.text = self.utente!.nome + " " + self.utente!.cognome
        labelNomeBarca.text = self.barca!.nome
        
        mapView.delegate = self
        
        self.navigationItem.hidesBackButton = true
        
        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters

        self.autorizza()
        
        self.inizializza()
                
        self.getListaPartecipanti()
        self.getListaBarche()
        
        self.aggiungiBussola()
    }
    
    func inizializza() {
//        let date = Format.dateWithTime(date: self.regata!.data, time: self.regata!.oraInizio)
//        _ = Timer(fireAt: date, interval: 0, target: self, selector: #selector(inizioGara), userInfo: nil, repeats: false)
        inizioGara()
        
        let finishDate = Format.dateWithTime(date: self.regata!.data, time: self.regata!.oraFine)
        _ = Timer(fireAt: finishDate, interval: 0, target: self, selector: #selector(fineGara), userInfo: nil, repeats: false)
        
        mapView.showsUserLocation = true
        mapView.userTrackingMode = .followWithHeading
    }
    
    func aggiungiBussola() {
        mapView.showsCompass = false
        
        let compassButton = MKCompassButton(mapView: mapView)
        compassButton.compassVisibility = .visible

        mapView.addSubview(compassButton)
        
        compassButton.translatesAutoresizingMaskIntoConstraints = false
        compassButton.trailingAnchor.constraint(equalTo: mapView.trailingAnchor, constant: -12).isActive = true
        compassButton.topAnchor.constraint(equalTo: mapView.topAnchor, constant: 12).isActive = true
    }
    
    ///Richiede l'autorizzazione per salvare la posizione.
    func autorizza() {
        let status = CLLocationManager.authorizationStatus()
        if status == .notDetermined || status == .denied || status == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    ///Imposta e inizia a salvare i dati della posizione.
    func iniziaAPrendereDati() {
        locationManager.delegate = self
        locationManager.activityType = .otherNavigation
        locationManager.distanceFilter = 10
        locationManager.startUpdatingLocation()
    }
    
    ///Smette di salvare i dati della posizione.
    func finisciDiPrendereDati() {
        locationManager.stopUpdatingLocation()
        timer?.invalidate()
        timer = nil
    }
    
    ///Aggiorna i dati delle label con dei dati formattati.
    func aggiornaDati() {
        let tempoFormattato = Format.time(seconds: tempo)
        timeLabel.text = "Tempo trascorso: " + tempoFormattato
    }

    ///Esegue le operazioni per cominciare la gara.
    @objc func inizioGara() {
        locationList.removeAll()
        mapView.removeOverlays(mapView.overlays)
        distance = Measurement(value: 0, unit: UnitLength.meters)
        tempo = 0
        aggiornaDati()
        ogniSecondo()
        ogniAttimo()

        iniziaAPrendereDati()
    }
    
    ///Esegue le operazioni per far finire la gara.
    @objc func fineGara() {
        self.finisciDiPrendereDati()
    }
    
    func ogniAttimo() {
        timer = Timer.scheduledTimer(timeInterval: Double(self.regata!.tempoAggiornamentoAltrui) ?? 30.0, target: self, selector: #selector(mandaPosizioni), userInfo: nil, repeats: true)
    }
    
    func ogniSecondo() {
        _ = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) {_ in
            self.tempo += 1
            self.aggiornaDati()
            self.aggiungiBussola()
        }
    }
    
    @objc func mandaPosizioni() {
        let longitude = String(locationList.last!.coordinate.longitude)
        let latitude = String(locationList.last!.coordinate.latitude)
        
        let data = DateTime.currentDate()
        let time = DateTime.currentTime()
        
        let url = URL(string: URLs.activeUrl + URLs.inviaPosizioneFileName)
        
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        restManager.httpBodyParameters.add(value: time, forKey: "ora")
        restManager.httpBodyParameters.add(value: self.utente!.idSkipper, forKey: "idSkipper")
        restManager.httpBodyParameters.add(value: data, forKey: "data")
        restManager.httpBodyParameters.add(value: longitude, forKey: "longitudine")
        restManager.httpBodyParameters.add(value: latitude, forKey: "latitudine")
        
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if 200...299 ~= results.response!.httpStatusCode {
                guard let data = results.data else { return }
                print(String(data: data, encoding: .utf8)!)
            }
        })
    }
    
    func getListaPartecipanti() {
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        
        let url = URL(string: URLs.activeUrl + URLs.listaUtentiFileName)
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if 200...299 ~= results.response!.httpStatusCode {
                guard let data = results.data else { return }
                let listaJSON = String(data: data, encoding: .utf8)!.components(separatedBy: "}")
                self.creaListaUtenti(listaJSON: listaJSON)
            }
        })
    }
    
    func getListaBarche() {
        restManager.requestHttpHeaders.add(value: "application/x-www-form-urlencoded", forKey: "Content-Type")
        
        let url = URL(string: URLs.activeUrl + URLs.listaBarcheFileName)
        restManager.makeRequest(toURL: url!, withHttpMethod: .post, completion: { (results) in
            if 200...299 ~= results.response!.httpStatusCode {
                guard let data = results.data else { return }
                let listaJSON = String(data: data, encoding: .utf8)!.components(separatedBy: "}")
                self.creaListaBarche(listaJSON: listaJSON)
            }
        })
    }
    
    func creaListaBarche(listaJSON: [String]) {
        var list = listaJSON
        list.removeLast()
        let jsonDecoder = JSONDecoder()
        for nave in list {
            let barca = nave + "}"
            listaBarche.append(try! jsonDecoder.decode(Barca.self, from: barca.data(using: .utf8)!))
        }
    }
    
    func creaListaUtenti(listaJSON: [String]) {
        var list = listaJSON
        list.removeLast()
        let jsonDecoder = JSONDecoder()
        for partecipante in list {
            let par = partecipante + "}"
            listaUtenti.append(try! jsonDecoder.decode(Utente.self, from: par.data(using: .utf8)!))
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destViewController = segue.destination as? PartecipantiTableViewController {
            destViewController.listaUtenti = self.listaUtenti
            destViewController.listaBarche = self.listaBarche
        }
    }
}

// MARK: Location Manager Delegate

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locations[locations.count - 1].speed * 3.6 / 1.852 <= 0 {
            speedLabel.text = "Velocità: " + String(format: "%.0f nodi", 0)
        } else {
            speedLabel.text = "Velocità: " + String(format: "%.0f nodi", locations[locations.count - 1].speed * 3.6 / 1.852)
        }
        
        for newLocation in locations {
            if let lastLocation = locationList.last {
                let delta = newLocation.distance(from: lastLocation)
                distance = distance + Measurement(value: delta, unit: UnitLength.meters)
                //Disegna la linea delle coordinate
                let coordinates = [lastLocation.coordinate, newLocation.coordinate]
                mapView.addOverlay(MKPolyline(coordinates: coordinates, count: 2))
            }
            locationList.append(newLocation)
        }
    }
}
